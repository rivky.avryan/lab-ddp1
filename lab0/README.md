# Lab0: Python Basic Syntax

Selamat datang di lab DDP1!

Pada Lab0 ini, kamu akan memulai perjalanan ngodingmu di DDP1 ini dengan Python. Kamu diminta untuk membuat beberapa program sederhana menggunakan `print()` dan `input()`, serta menyimpan histori pengerjaanmu menggunakan GitLab. Selamat mencoba!

---

## Task 1: Intro to Git & Basic Python

Andaikan kamu memiliki sebuah program yang sudah bekerja. Kemudian kamu melanjutkan kodinganmu karena ingin menambahkan fitur baru, namun ternyata kodingan barumu salah dan akhirnya programmu benar-benar tidak bisa dijalankan. Kamu pun berharap bisa me-*restore* versi kodinganmu sebelumnya yang masih bekerja. Hal inilah yang dapat dilakukan oleh Git, sebuah *Version Control System*. Kamu juga dapat mengunggah keseluruhan programmu yang menggunakan Git ke situs penyedia layanan *hosting* repositori Git, salah satunya adalah yang akan dipakai dalam pengerjaan lab ini, yaitu GitLab.

Pada Task 1 ini, kamu diminta untuk membuat sebuah program sederhana berisi profilmu dengan melakukan langkah-langkah berikut.

1. Install [git](https://git-scm.com/downloads) terlebih dahulu.

2. Buka repositori lab DDP1 pada https://gitlab.com/ddp1-genap-2021-2022/lab-ddp1. *Copy* repositori milik akun GitLab DDP1 ke akun GitLab milikmu dengan menekan tombol **`fork`**.

    <img src="images/task1-01.png" width="60%">

3. Pastikan repositori berhasil di-*fork* dengan melihat *username* dan namamu sebagai *owner*. Unduh repositori tersebut ke komputermu dengan menekan tombol **`clone`** dan salin URL HTTPS (SSH memerlukan setup khusus).

    <img src="images/task1-02.png" width="60%">

4. Buka cmd dan masuk ke direktori yang diinginkan untuk menyimpan repo tersebut. Lalu, jalankan perintah **`git clone <URL>`** menggunakan URL yang telah disalin sebelumnya dan masuk ke direktori tersebut.

    <img src="images/task1-03.png" width="45%">

5. Buka *text editor* atau IDE favoritmu, kemudian buat file baru bernama **profil.py** pada folder lab0. 
    
    - Buatlah variabel `nama`, `tanggal`, `bulan`, `tahun`, `umur`, dan `email`
    
    - Isi variabel: 
        
        - `nama` dengan nama lengkapmu
        
        - `tanggal`, `bulan`, dan `tahun` dengan tanggal, bulan, dan tahun lahirmu
        
        - `umur` dengan penggunaan aritmatika tahun sekarang dikurangi tahun lahirmu (variabel tahun)
        
        - `email` dengan alamat emailmu
    
    - Cetak (*print*) profilmu dengan format berikut.
        ```
        Profil Saya
        Nama: nama
        Tanggal Lahir: tanggal bulan tahun
        Umur: umur
        Email: email
        ```
        
        Contoh program:
        ```
        nama = “Annisa Dian N”
        tanggal = 7
        bulan = “September”
        tahun = 2001
        umur = 2022 - tahun
        email = “adn@gmail.com”

        print(“Profil Saya”)
        print(“Nama: ” + nama)
        print(“Tanggal Lahir: ” + str(tanggal) + “ ” + bulan + “ ” + str(tahun))
        print(“Umur: ” + str(umur))
        print(“Email: ” + email)
        ```
        Catatan: `str()` akan mengubah tipe data menjadi string

        Contoh output:
        ```
        Profil Saya
        Nama: Annisa Dian N
        Tanggal Lahir: 7 September 2001
        Umur: 21
        Email: adn@gmail.com
        ```

6. Untuk memeriksa file yang termodifikasi ataupun file baru di komputermu, jalankan perintah **`git status`**.

    <img src="images/task1-04.png" width="40%">

7. Sebelum menyimpan perubahan yang kamu lakukan, tentukan dahulu file mana saja yang ingin disimpan perubahannya. Pada kasus ini, karena hanya file profil.py saja yang berubah, maka jalankan perintah **`git add lab0/profil.py`**. Cek kembali menggunakan **`git status`** dan pastikan perubahan tersebut sudah ada di *staging* (berwarna hijau) dan siap untuk di-*commit*.

    <img src="images/task1-05.png" width="40%">

    Catatan: Ada beberapa cara lain untuk melakukan **`git add`**
    
    - `git add *` : menambahkan semua file ke *staging*
    
    - `git add .` : menambahkan semua file pada direktori saat ini serta subdirektorinya ke *staging*
    
    - `git add -u` : menambahkan hanya file yang berubah (bukan file baru) ke *staging*

8. Sebelum melakukan *commit*, pastikan bahwa kamu sudah mengatur *username* dan *email* git yang sesuai dengan akun GitLab-mu di komputermu. Hal ini diperlukan untuk menandai bahwa *commit* tersebut dibuat olehmu. Untuk mengatur *username* dan *email* git, gunakan perintah berikut.

    `git config --global user.name "Nama Kamu"`
    
    `git config --global user.email "emailmu@server"`

    <img src="images/task1-06.png" width="60%">
    
    Catatan: Hilangkan parameter `--global` jika ingin mengatur konfigurasi hanya untuk repositori tersebut.

9. Simpan perubahan yang telah kamu lakukan dengan menjalankan **`git commit -m "<message>"`**, contohnya: `git commit -m "Lab0: Task 1 done"`

    <img src="images/task1-15.png" width="40%">

10. Unggah perubahan ke repositori *online*-mu pada GitLab dengan menjalankan **`git push origin master`**. Jika diminta *username/email* dan *password*, gunakan detail akun GitLab-mu. 

    <img src="images/task1-07.png" width="40%">

11. Setelah di-*push*, semua *commit* yang telah kamu lakukan akan ditampilkan pada bagian *commits* pada halaman repositori GitLab-mu.

    <img src="images/task1-08.png" width="30%">

### Tambahan

1. Jika nantinya ada soal baru ataupun revisi soal.
    
    Kamu bisa mendapat pembaruan dengan menambahkan dahulu repositori GitLab milik DDP1 sebagai `upstream`. Jalankan **`git remote add upstream <URL>`** dan pastikan upstream berhasil ditambahkan.

    <img src="images/task1-09.png" width="60%">

    Nantinya, kamu dapat mendapat pembaruan soal atau revisi menggunakan perintah **`git pull upstream master`**

2. GitLab sebagai Version Control System

    Kamu dapat melihat log perubahan yang pernah dilakukan dengan menjalankan **`git log`**. Pilih versi yang diinginkan dengan menyalin *commit hash* (cukup beberapa karakter pertama), lalu keluar dari `git log` dengan menekan karakter `q` jika diperlukan. Kemudian, lakukan **`git checkout <commit hash>`** ke versi tersebut.

    <img src="images/task1-10.png" width="50%">

    <img src="images/task1-11.png" width="40%">

    Lihat kembali filemu dan pastikan perubahan yang telah kamu lakukan sebelumnya tidak ada (tidak ada file profil.py).

    <img src="images/task1-12.png" width="15%">

    Kamu dapat kembali ke versi terbaru menggunakan perintah **`git checkout master`** dan pastikan perubahan yang telah kamu kerjakan sudah ada kembali.

    <img src="images/task1-13.png" width="40%">

    <img src="images/task1-14.png" width="15%">

Sebenarnya masih banyak fungsi Git lain yang dapat kamu manfaatkan, namun tidak dibahas karena belum diperlukan untuk pengerjaan lab DDP1 ini. Misalnya `git branch`, yang dimanfaatkan untuk kolaborasi dengan *developer* lain. Meski demikian, akan lebih baik apabila kamu mengeksplorasi Git lebih dalam karena akan sangat berguna dalam mata kuliah ataupun kegiatan lain di masa depanmu.

---

## Task 2: Mulai Kuliah

Minggu ini adalah minggu pertama kuliah. Pada pertemuan pertama perkuliahan, saat kamu ingin menulis catatan, ternyata kamu baru sadar bahwa stok alat tulismu habis sehingga kamu perlu membeli alat tulis baru. Buatlah program sederhana yang mencatat pembelianmu tersebut.

Terdapat 3 barang yang tersedia di toko terdekat rumahmu, yaitu pulpen, pensil, dan *correction tape*. Kamu juga sudah hafal dengan masing-masing harganya sehingga programmu bisa menunjukkan langsung daftar barang serta harganya di toko tersebut. Kemudian, programmu juga perlu menanyakan jumlah dari masing-masing barang yang ingin dibeli. Lalu di akhir program, kamu perlu menunjukkan ringkasan pembelian serta total harga yang perlu dibayar.

Contoh masukan:
```
Toko ini menjual:
pulpen (10000/pcs)
pensil (5000/pcs)
correction tape (15000/pcs)
Masukkan jumlah yang ingin dibeli
pulpen: 2
pensil: 1
correction tape: 0
```

Contoh keluaran:
```
Ringkasan pembelian:
2 pulpen x 10000
1 pensil x 5000
0 correction tape x 15000
Total harga: 25000
```

Langkah pengerjaan:
1. Buatlah file baru bernama **alat_tulis.py** pada folder lab0

2. Buatlah variabel harga pulpen, pensil, dan correction tape, serta isi masing-masing dengan harga yang kamu inginkan (bebas)

    Contoh kode:

    <img src="images/task2-01.png" width="25%">

3. Cetak dahulu daftar barang dan harga dari masing-masing barang

    Contoh kode:

    <img src="images/task2-02.png" width="45%">

4. Buatlah variabel baru untuk menyimpan jumlah masing-masing barang yang ingin dibeli
    
    Contoh kode:

    <img src="images/task2-03.png" width="40%">

    Catatan: int() untuk mengubah data type menjadi integer

5. Hitung total harga dengan mengalikan jumlah dengan harga setiap barang

    `Total harga = (jumlah pulpen * harga pulpen) + (jumlah pensil * harga pensil) + (jumlah correction tape * harga correction tape)`

6. Cetak ringkasan pembelian sebagai keluaran
    
    Contoh kode:

    <img src="images/task2-04.png" width="40%">

7. Lakukan kembali proses *add*, *commit* (*commit message* dibebaskan), serta *push* ke repositori GitLab kamu.

---

## Task 3: Refleksi

Untuk mengevaluasi hasil belajarmu pada Lab0 ini, buatlah file *markdown* dengan nama **refleksi.md** pada folder lab0 dan jawablah pertanyaan-pertanyaan berikut.

1. Apa hal baru yang kamu pelajari melalui lab ini?
2. Adakah kesulitan yang kamu alami saat mencoba git? Jelaskan.
3. Hal apa yang masih sulit kamu pahami?

Lakukan kembali proses *add*, *commit* (*commit message* dibebaskan), serta *push* ke repositori GitLab kamu.

## Pengumpulan

Berikut adalah daftar file yang perlu dikumpulkan dan telah diletakkan dalam direktori lab0 pada repositori GitLab kalian masing-masing.

- profil.py
- alat_tulis.py
- refleksi.md

Contoh struktur direktori:

<img src="images/dir.png" width="15%">